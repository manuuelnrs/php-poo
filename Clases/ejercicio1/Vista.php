<?php
include 'Carro.php';
include 'Moto.php';
?>

<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="author" content="Juan Manuel Nava Rosales">
    <title>Moto</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  </head>

  <body class="bg-light">

    <main role="main" class="container">
      <div class="my-3 p-3 bg-light rounded shadow-sm">
        <div class="container col-md-10">
          <form id="form-registro" method="POST">

            <div class="form-group">
              <label for="color">Color</label>
              <input type="text" class="form-control" id="color" name="color" placeholder="Color" require>
            </div>

						<div class="form-group">
              <label for="cilindraje">Cilindraje</label>
              <input type="number" class="form-control" id="cilindraje" name="cilindraje" placeholder="Cilindraje" require>
            </div>

            <button type="submit" class="btn btn-primary" title="Registrar" name="Registrar" value="Registrar">Registrar</button>

          </form>
        </div>        
      </div>
    </main>

  </body>
</html>