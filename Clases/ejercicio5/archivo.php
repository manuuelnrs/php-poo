<?php  
//declaracion de clase contraseña
	class contraseña {
		//declaracion de atributos
		private $nombre;
		private $contraseña;

		//declaracion de metodo constructor
		public function __construct($nombre_front){
			$this->nombre = $nombre_front;
			$this->contraseña = $this->random_pass();
		}

		private function random_pass(){
			$patron = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			$pass = '';
			for( $i = 0; $i < 4; $i++ )
					$pass .= substr($patron, mt_rand(0, 25), 1);
			return $pass;
		}

		//declaracion del metodo mostrar para armar el mensaje con el nombre y contraseña
		public function mostrar(){
			return 'Hola '.$this->nombre.' esta es tu contraseña: '.$this->contraseña;
		}

		//declaracion de metodo destructor
		public function __destruct(){
			//destruye contraseña
			$this->contraseña='La contraseña ha sido destruida';
			echo $this->contraseña;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$contra1 = new contraseña($_POST['nombre']);
	$mensaje = $contra1->mostrar();
}


?>
