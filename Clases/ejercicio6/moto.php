<?php
include_once('transporte.php');

//declaracion de la clase hijo o subclase Moto
class moto extends transporte{
  // Atributo propio de una Moto
  private $cilindraje;

  //declaracion de constructor
  public function __construct($nom,$vel,$com,$cc){
    //sobreescritura de constructor de la clase padre
    parent::__construct($nom,$vel,$com);
    $this->cilindraje = $cc;
  }

  // declaracion de metodo
  public function resumenMoto(){
    // sobreescribitura de metodo crear_ficha en la clse padre
    $mensaje=parent::crear_ficha();
    $mensaje.='<tr>
          <td>Cilindraje:</td>
          <td>'. $this->cilindraje.'</td>				
        </tr>';
    return $mensaje;
  }
}

?>