<?php
//creación de la clase carro
class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	private $circula;

	//declaracion del método verificación
	public function verificacion( $añofabricacion ){
		$añofabricacion = substr($añofabricacion, 0, 4); // Tomar el año de la fecha

		if( $añofabricacion < 1990 ){
			$this->circula = "No";
		}elseif( $añofabricacion >= 1990 and $añofabricacion <= 2010 ){
			$this->circula = "Revisión";
		}else{
			$this->circula = "Si";
		}
	}

	public function getResVerificacion(){
		return $this->circula; // Devolver el valor del atributo privado circula
	}
}

//creación de instancia a la clase Carro
$Carro1 = new Carro2();

if (!empty($_POST)){
	$Carro1->color=$_POST['color'];
	$Carro1->modelo=$_POST['modelo'];
	$Carro1->verificacion($_POST['añofabricacion']);
}




